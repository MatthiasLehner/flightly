package com.pomography.pom.flightly;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by matthiaslehner on 29.05.17.
 */

public class Flight implements Serializable {

    public class Departure implements Serializable {

        String airportCode;
        Date scheduledTimeLocal;
        Date scheduledTimeUtc;
        String timeStatus;
        String terminal;

        public Departure(String airportCode, Date scheduledTimeLocal, Date scheduledTimeUtc, String timeStatus, String terminal) {
            this.airportCode = airportCode;
            this.scheduledTimeLocal = scheduledTimeLocal;
            this.scheduledTimeUtc = scheduledTimeUtc;
            this.timeStatus = timeStatus;
            this.terminal = terminal;
        }

        public Departure(JSONObject json) throws JSONException {
            this.airportCode = json.getString("AirportCode");

            JSONObject scheduledTimeLocal = json.getJSONObject("ScheduledTimeLocal");

            Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");

            try {
                this.scheduledTimeLocal = (Date)((DateFormat) formatter).parse(scheduledTimeLocal.getString("DateTime"));
            } catch (ParseException exception) {
                Log.e("FlightActivity", exception.toString());
            }

            JSONObject scheduledTimeUtc = json.getJSONObject("ScheduledTimeUTC");

            formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm'Z'");

            try {
                this.scheduledTimeUtc = (Date)((DateFormat) formatter).parse(scheduledTimeUtc.getString("DateTime"));
            } catch (ParseException exception) {
                Log.e("FlightActivity", exception.toString());
            }
            this.timeStatus = json.getString("TimeStatus");

            JSONObject terminal = json.getJSONObject("Terminal");

            this.terminal = terminal.getString("Name");
        }
    }

    public class Arrival implements Serializable {
        String airportCode;
        Date scheduledTimeLocal;
        Date scheduledTimeUtc;
        String timeStatus;
        String terminal;

        public Arrival(String airportCode, Date scheduledTimeLocal, Date scheduledTimeUtc, String timeStatus, String terminal) {
            this.airportCode = airportCode;
            this.scheduledTimeLocal = scheduledTimeLocal;
            this.scheduledTimeUtc = scheduledTimeUtc;
            this.timeStatus = timeStatus;
            this.terminal = terminal;
        }

        public Arrival(JSONObject json) throws JSONException {
            this.airportCode = json.getString("AirportCode");

            JSONObject scheduledTimeLocal = json.getJSONObject("ScheduledTimeLocal");

            Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");

            try {
                Log.d("Flight", scheduledTimeLocal.getString("DateTime"));
                this.scheduledTimeLocal = (Date)((DateFormat) formatter).parse(scheduledTimeLocal.getString("DateTime"));
                Log.d("Flight", this.scheduledTimeLocal.toString());
            } catch (ParseException exception) {
                Log.e("FlightActivity", exception.toString());
            }

            JSONObject scheduledTimeUtc = json.getJSONObject("ScheduledTimeUTC");

            formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm'Z'");

            try {
                Log.d("Flight", scheduledTimeUtc.getString("DateTime"));
                this.scheduledTimeUtc = (Date)((DateFormat) formatter).parse(scheduledTimeUtc.getString("DateTime"));
            } catch (ParseException exception) {
                Log.e("FlightActivity", exception.toString());
            }
            this.timeStatus = json.getString("TimeStatus");

            JSONObject terminal = json.getJSONObject("Terminal");

            this.terminal = terminal.getString("Name");
        }
    }

    public class MarketingCarrier implements Serializable {
        String airlineID;
        String flightNumber;

        public MarketingCarrier(String airlineID, String flightNumber) {
            this.airlineID = airlineID;
            this.flightNumber = flightNumber;
        }

        public MarketingCarrier(JSONObject json) throws JSONException {
            this.airlineID = json.getString("AirlineID");
            this.flightNumber = json.getString("FlightNumber");
        }
    }

    public class OperatingCarrier implements Serializable {
        String airlineID;
        String flightNumber;

        public OperatingCarrier(String airlineID, String flightNumber) {
            this.airlineID = airlineID;
            this.flightNumber = flightNumber;
        }

        public OperatingCarrier(JSONObject json) throws JSONException {
            this.airlineID = json.getString("AirlineID");
            this.flightNumber = json.getString("FlightNumber");
        }
    }

    public class Equipment implements Serializable {
        String aircraftCode;

        public Equipment(String aircraftCode) {
            this.aircraftCode = aircraftCode;
        }

        public Equipment(JSONObject json) throws JSONException {
            this.aircraftCode = json.getString("AircraftCode");
        }
    }

    public class FlightStatus implements Serializable {
        String code;
        String definition;

        public FlightStatus(String code, String definition) {
            this.code = code;
            this.definition = definition;
        }

        public FlightStatus(JSONObject json) throws JSONException {
            this.code = json.getString("Code");
            this.definition = json.getString("Definition");
        }
    }

    Departure departure;
    Arrival arrival;
    MarketingCarrier marketingCarrier;
    OperatingCarrier operatingCarrier;
    Equipment equipment;
    FlightStatus flightStatus;

    public Flight(Departure departure, Arrival arrival, MarketingCarrier marketingCarrier, OperatingCarrier operatingCarrier, Equipment equipment, FlightStatus flightStatus) {
        this.departure = departure;
        this.arrival = arrival;
        this.marketingCarrier = marketingCarrier;
        this.operatingCarrier = operatingCarrier;
        this.equipment = equipment;
        this.flightStatus = flightStatus;
    }

    public Flight(JSONObject json) throws JSONException {
        JSONObject flightStatusResourcejson = json.getJSONObject("FlightStatusResource");
        JSONObject flights = flightStatusResourcejson.getJSONObject("Flights");
        JSONObject flight = flights.getJSONObject("Flight");

        this.departure = new Departure(flight.getJSONObject("Departure"));
        this.arrival = new Arrival(flight.getJSONObject("Arrival"));
        this.marketingCarrier = new MarketingCarrier(flight.getJSONObject("MarketingCarrier"));
        this.operatingCarrier = new OperatingCarrier(flight.getJSONObject("OperatingCarrier"));
        this.equipment = new Equipment(flight.getJSONObject("Equipment"));
        this.flightStatus = new FlightStatus(flight.getJSONObject("FlightStatus"));
    }

    public String toString() {
        return null;
    }

}
