package com.pomography.pom.flightly;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FlightActivity extends AppCompatActivity {

    private TextView overviewTextView;
    private TextView departureTextView;
    private TextView arrivalTextView;
    private TextView departureTerminalTextView;
    private TextView arrivalTerminalTextView;
    private TextView airlineTextView;
    private TextView flightTextView;

    private Flight flight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);

        overviewTextView = (TextView) findViewById(R.id.overview_text_view);
        departureTextView = (TextView) findViewById(R.id.departure_text_view);
        arrivalTextView = (TextView) findViewById(R.id.arrival_text_view);
        departureTerminalTextView = (TextView) findViewById(R.id.departure_terminal_text_view);
        arrivalTerminalTextView = (TextView) findViewById(R.id.arrival_terminal_text_view);
        airlineTextView = (TextView) findViewById(R.id.airline_text_view);
        flightTextView = (TextView) findViewById(R.id.flight_text_view);

        DateFormat formatter = new SimpleDateFormat("hh:mm yyyy-MM-dd");

        try {
            this.flight = (Flight) getIntent().getSerializableExtra("flight");

            this.overviewTextView.setText(this.flight.departure.airportCode + " \u2708 " + this.flight.arrival.airportCode);
            this.departureTextView.setText(formatter.format(this.flight.departure.scheduledTimeUtc));
            this.arrivalTextView.setText(formatter.format(this.flight.departure.scheduledTimeLocal));
            this.departureTerminalTextView.setText(this.flight.departure.terminal);
            this.arrivalTerminalTextView.setText(this.flight.arrival.terminal);
            this.airlineTextView.setText(this.flight.operatingCarrier.airlineID);
            this.flightTextView.setText(this.flight.operatingCarrier.flightNumber);
        } catch(ClassCastException exception) {
            Log.e("FlightActivity", exception.toString());
        }
    }
}
