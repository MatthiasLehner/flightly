package com.pomography.pom.flightly;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FlightService {

    public abstract static class FlightStatusCallback {
        abstract void onDataReceived(Flight flight);
    }

    private final String FLIGHT_STATUS_URL = "https://api.lufthansa.com/v1/operations/flightstatus/";

    private final RequestQueue queue;

    public FlightService(Context context) {
        this.queue = Volley.newRequestQueue(context);
    }

    public void getFlightStatus(final String token, final String flightNumber, String date, final FlightStatusCallback callback) {

        Log.d("SearchActivity", "test2");

        String url = this.FLIGHT_STATUS_URL + flightNumber + "/" + date + "/";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("FLightService", "SUCCESS");
                        try {
                            Log.d("FLightService", "SUCCESS");
                            Flight flight = new Flight(response);
                            callback.onDataReceived(flight);
                        } catch (JSONException exception) {
                            Log.e("FlightService", exception.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("FlightService", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer" + " " + token);

                return params;
            }
        };

        this.queue.add(request);
    }
}
