package com.pomography.pom.flightly;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class SearchActivity extends AppCompatActivity {

    private final String TOKEN = "wjrtut4x8dkf5cjdh8gr6jbj";

    private EditText flightNumberEditView;
    private EditText flightDateEditView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        flightNumberEditView = (EditText) findViewById(R.id.flight_number_text_view);
        flightDateEditView = (EditText) findViewById(R.id.flight_date_text_view);
    }

    public void onClick(View v) {
        /*
        Intent k = new Intent(this, FlightsActivity.class);
        startActivity(k);
        */

        FlightService service = new FlightService(this);

        service.getFlightStatus(TOKEN, flightNumberEditView.getText().toString(), flightDateEditView.getText().toString(), new FlightService.FlightStatusCallback () {

            @Override
            void onDataReceived(Flight flight) {
                Intent intent = new Intent(getBaseContext(), FlightActivity.class);
                intent.putExtra("flight", flight);
                startActivity(intent);
            }
        });
    }
}
